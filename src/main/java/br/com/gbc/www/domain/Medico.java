package br.com.gbc.www.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Medico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;
    private String crm;
    private String telefone;
    private String celular;
    private String cep;

    @ManyToMany
    @JoinTable(name = "medico_especialidade", joinColumns =
            {@JoinColumn(name = "medico_id")}, inverseJoinColumns =
            {@JoinColumn(name = "especialidade_id")})
    private List<Especialidade> especialidades;
}
