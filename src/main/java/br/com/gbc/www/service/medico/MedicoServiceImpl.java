package br.com.gbc.www.service.medico;

import br.com.gbc.www.domain.Especialidade;
import br.com.gbc.www.domain.Medico;
import br.com.gbc.www.repository.MedicoRepository;
import br.com.gbc.www.requests.MedicoRequestBody;
import br.com.gbc.www.service.especialidade.EspecialidadeServiceImpl;
import br.com.gbc.www.specifications.MedicoSpesification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicoServiceImpl implements MedicoService {

    private final MedicoRepository medicoRepository;
    private final EspecialidadeServiceImpl especialidadeService;

    public MedicoServiceImpl(MedicoRepository medicoRepository, EspecialidadeServiceImpl especialidadeService) {
        this.medicoRepository = medicoRepository;
        this.especialidadeService = especialidadeService;
    }

    @Override
    public Medico salvarMedico(MedicoRequestBody medicoRequest) {
        return medicoRepository.save(convertResquestToEntity(medicoRequest));
    }

    @Override
    public List<Medico> listarMedicos() {
        return medicoRepository.findAll();
    }

    @Override
    public List<Medico> listarMedicosComParametros(String nome, String crm, String cep) {
        List<Medico> medicos = medicoRepository.findAll(MedicoSpesification.buscarComParametrosDinamicos(nome, crm, cep));
        return medicos;
    }

    private Medico convertResquestToEntity(MedicoRequestBody medicoRequest) {
        List<Especialidade> especialidades = especialidadeService.listarEspecialidades(medicoRequest.getCodigosEspecialidades());
        return Medico
                .builder()
                .nome(medicoRequest.getNome())
                .celular(medicoRequest.getCelular())
                .cep(medicoRequest.getCep())
                .crm(medicoRequest.getCrm())
                .telefone(medicoRequest.getTelefone())
                .especialidades(especialidades)
                .build();
    }

}
