package br.com.gbc.www.service.especialidade;

import br.com.gbc.www.domain.Especialidade;
import br.com.gbc.www.repository.EspecialidadeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspecialidadeServiceImpl implements EspecialidadeService {

    private final EspecialidadeRepository especialidadeRepository;

    public EspecialidadeServiceImpl(EspecialidadeRepository especialidadeRepository) {
        this.especialidadeRepository = especialidadeRepository;
    }

    @Override
    public Especialidade buscarEspecialidade(Integer codidoEspecialidade) {
        Especialidade especialidadeOpt = especialidadeRepository
                .findById(codidoEspecialidade).orElseThrow(() -> new RuntimeException("Especialidade não encontrada"));
        return especialidadeOpt;
    }

    @Override
    public List<Especialidade> listarEspecialidades(List<Integer> codigosEspecialidades) {
        return especialidadeRepository.findAllById(codigosEspecialidades);
    }
}
