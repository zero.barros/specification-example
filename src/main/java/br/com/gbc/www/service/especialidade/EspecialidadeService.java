package br.com.gbc.www.service.especialidade;

import br.com.gbc.www.domain.Especialidade;

import java.util.List;

public interface EspecialidadeService {

    Especialidade buscarEspecialidade(Integer codidoEspecialidade);

    List<Especialidade> listarEspecialidades(List<Integer> codigosEspecialidades);
}
