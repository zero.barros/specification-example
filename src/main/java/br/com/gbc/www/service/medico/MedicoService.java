package br.com.gbc.www.service.medico;

import br.com.gbc.www.domain.Medico;
import br.com.gbc.www.requests.MedicoRequestBody;

import java.util.List;

public interface MedicoService {

    Medico salvarMedico(MedicoRequestBody medicoRequest);

    List<Medico> listarMedicos();

    List<Medico> listarMedicosComParametros(String nome, String crm, String cep);
}
