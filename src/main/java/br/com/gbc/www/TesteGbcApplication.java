package br.com.gbc.www;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteGbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteGbcApplication.class, args);
	}

}
