package br.com.gbc.www.requests;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MedicoRequestBody {

    private String nome;
    private String crm;
    private String telefone;
    private String celular;
    private String cep;
    private List<Integer> codigosEspecialidades;
}
