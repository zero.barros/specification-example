package br.com.gbc.www.specifications;

import br.com.gbc.www.domain.Medico;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class MedicoSpesification {

    public static Specification<Medico> buscarComParametrosDinamicos(String nome, String crm, String cep) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (nome != null){
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("nome")), "%"+ nome.toLowerCase() + "%"));
            }
            if (crm != null){
                predicates.add(criteriaBuilder.or(criteriaBuilder.equal(root.get("crm"), crm)));
            }
            if (cep != null){
                predicates.add(criteriaBuilder.or(criteriaBuilder.equal(root.get("cep"), cep)));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }


}
