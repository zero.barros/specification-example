package br.com.gbc.www.controller;


import br.com.gbc.www.domain.Medico;
import br.com.gbc.www.requests.MedicoRequestBody;
import br.com.gbc.www.service.medico.MedicoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {

    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @GetMapping
    public ResponseEntity<List<Medico>> listarMedicos() {
        return ResponseEntity.ok(medicoService.listarMedicos());
    }

    @GetMapping("/param")
    public ResponseEntity<List<Medico>> listarMedicosComParametros(
            @RequestParam (value = "nome", required = false)  String nome,
            @RequestParam (value = "crm", required = false)  String crm,
            @RequestParam (value = "cep", required = false)  String cep) {
        List<Medico> medicos = medicoService.listarMedicosComParametros(nome, crm, cep);
        return ResponseEntity.ok(medicos);

    }

    @PostMapping
    public ResponseEntity<Medico> salvarMedico(@RequestBody MedicoRequestBody medicoRequestBody) {
        return new ResponseEntity<>(medicoService.salvarMedico(medicoRequestBody), HttpStatus.CREATED);
    }


}
